module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "Продажа и аренда строительных лесов",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { name: "author", content: "Nikita Egorov from BCS" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    link: [
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Righteous&display=swap"
      }
    ]
  },

  /*
   * CSS
   */

  css: [
    "~assets/fonts/icomoon/style.css",
    "~assets/fonts/flaticon/font/flaticon.css",
    "~assets/css/bootstrap.min.css",
    "~assets/css/aos.css",
    "~assets/css/style.css"
  ],
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  }
};
